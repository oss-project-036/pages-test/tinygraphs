module github.com/taironas/tinygraphs

go 1.18

require (
	github.com/ajstarks/svgo v0.0.0-20210927141636-6d70534b1098
	github.com/taironas/route v0.0.0-20150312082541-b919c49c73f1
)
